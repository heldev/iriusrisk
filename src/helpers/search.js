const arraySearch = (criteria) => {
    return ({ [criteria]: a }, { [criteria]: b }) => {
        if(a === b) {
            return 0;
        }
        if(a < b) {
            return -1;
        }
        return 1;
    }
};

export {
    arraySearch
}
