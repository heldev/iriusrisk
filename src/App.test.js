import { render } from '@testing-library/react';
import App from './App';

test('renders the App', () => {
  const { container } = render(<App />);
  const AppWrapper = container.querySelector('div.App');
  expect(AppWrapper).toBeInTheDocument();
});
