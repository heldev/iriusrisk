import { useEffect } from 'react';
import { useSelector } from 'react-redux'
import { selectShips, selectTotalShips } from 'domain/starship/store/starshipSelector';
import PageHeader from 'components/PageHeader';
import Sort from 'components/Sort';
import Search from 'components/Search';
import starshipManager from 'domain/starship/manager/starshipManager';
import ProductGrid from 'components/ProductGrid';
import Paginator from 'components/Paginator';
import { SORTING_CRITERIA } from 'domain/starship/constants';

function Starships() {
    const starships = useSelector(selectShips);
    const totalShips = useSelector(selectTotalShips);
    const searchTerms = useSelector(state => state.starships.searchTerms)

    useEffect( () => {
        starshipManager().getAllStarShips();
    }, []);

    return (
        <div className="page">
            <PageHeader title="Starships">
                <Sort options={SORTING_CRITERIA} onChange={starshipManager().setOrderCriteria}  />
                <Search value={searchTerms} onChange={starshipManager().setSearchTerms} clearSearch={starshipManager().clearSearchTerms} />
            </PageHeader>
            <ProductGrid items={ starships } />
            <Paginator
                prevAction={starshipManager().previousPage}
                nextAction={starshipManager().nextPage}
                itemsName="starships"
                first={starshipManager().getFirstItem(totalShips)}
                last={starshipManager().getLastItem(totalShips)}
                total={totalShips}
            />
        </div>
    );
}

export default Starships;
