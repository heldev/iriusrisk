const SORTING_CRITERIA = [
    {
        key: null,
        label: 'No order'
    },
    {
        key: 'name',
        label: 'Name'
    },
    {
        key: 'cargo_capacity',
        label: 'Cargo Capacity'
    },
    {
        key: 'crew',
        label: 'Crew'
    }
];

export {
    SORTING_CRITERIA
};
