import axios from 'axios';
import store from 'store';
import { REST_ACTIONS } from 'constants/rest';

export default function starshipRepository () {
    return {
        fetchStarShips: async () => {
            const apiPage = store.getState().starships.apiPage;
            return axios.get(`${REST_ACTIONS.GET_STARSHIPS}?page=${apiPage}`);
        }
    }
}
