import starShipModel from '../model/starShipModel';

export default function starshipNormalizer () {
    return {
        normalizeStarShips: ({ data }) => {
            const { results : starships } = data;
            return {
                starships: starships.map(starShipModel)
            };
        }
    }
}
