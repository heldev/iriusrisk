import { createSlice } from '@reduxjs/toolkit';

export const starshipSlice = createSlice({
    name: 'starships',
    initialState: {
        list: [],
        page: 1,
        apiPage: 0,
        searchTerms: '',
        orderCriteria: null
    },
    reducers: {
        setTotal: (state, action) => {
            state.total = action.payload;
        },
        setStarships: (state, action) => {
            action.payload.forEach(
                (item) => {
                    const index = state.list.findIndex(({ id }) => item.id === id);
                    if (index === -1) {
                        state.list.push(item);
                    } else {
                        state.list[index] = item;
                    }
                }
            );
        },
        increasePage: (state) => {
            state.page++;
        },
        decreasePage: (state) => {
            state.page--;
        },
        resetPaginator: (state) => {
            state.page = 1;
        },
        increaseApiPage: (state) => {
            state.apiPage++;
        },
        decreaseApiPage: (state) => {
            state.apiPage--;
        },
        setSearchTerms: (state, action) => {
            state.searchTerms = action.payload;
        },
        setOrderCriteria: (state, action) => {
            state.orderCriteria = action.payload;
        }
    }
})

export const { setStarships, setTotal, increasePage, decreasePage, resetPaginator, increaseApiPage, decreaseApiPage, setSearchTerms, setOrderCriteria } = starshipSlice.actions;

export default starshipSlice.reducer;
