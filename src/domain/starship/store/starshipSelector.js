import { createSelector } from '@reduxjs/toolkit'
import { PAGE_SIZE } from 'constants/rest';
import { arraySearch } from 'helpers/search';

const starshipsList = state => state.starships.list;
const starshipsPage = state => state.starships.page;
const starshipsOrderCriteria = state => state.starships.orderCriteria;
const starshipsSearchTerms = state => state.starships.searchTerms;

const selectShips = createSelector([starshipsList, starshipsPage, starshipsOrderCriteria, starshipsSearchTerms], (list, page, orderCriteria, searchTerms) => {
    return list
        .filter(({ name }) => name.includes(searchTerms))
        .sort(arraySearch(orderCriteria))
        .slice((page - 1) * PAGE_SIZE, page * PAGE_SIZE);
});

const selectTotalShips = createSelector([starshipsList, starshipsPage, starshipsSearchTerms], (list, page, searchTerms) => {
    return list
        .filter(({ name }) => name.includes(searchTerms))
        .length
});

export {
    selectShips,
    selectTotalShips
};
