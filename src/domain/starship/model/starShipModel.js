import starshipManager from '../manager/starshipManager';
export default function starShipModel (data) {
    return {
        name: data.name,
        url: data.url,
        id: parseInt(starshipManager().parseId(data.url)),
        crew: parseInt(data.crew),
        image: `images/starships/${starshipManager().parseId(data.url)}.png`,
        cargo_capacity: parseInt(data.cargo_capacity)
    }
}
