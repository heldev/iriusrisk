import store from 'store';
import { setStarships, setTotal, increasePage, decreasePage, resetPaginator, decreaseApiPage, increaseApiPage, setSearchTerms, setOrderCriteria } from '../store/starshipSlice';
import starshipRepository from '../repository/starshipRepository';
import starshipNormalizer from '../normalizer/starshipNormalizer';
import { PAGE_SIZE } from 'constants/rest';

export default function starshipManager () {
    return {
        getAllStarShips: async () => {
            starshipManager().nextApiPage();
            await starshipManager().getStarShips() && await starshipManager().getAllStarShips();
        },
        getStarShips: async () => {
            try {
                const starships = await starshipRepository().fetchStarShips();
                const data = starshipNormalizer().normalizeStarShips(starships);
                starshipManager().saveToStore(data);
                return true;
            } catch (e) {
                return false;
            }
        },
        saveToStore: ({ starships, total }) => {
            store.dispatch(setStarships(starships));
            store.dispatch(setTotal(total));
        },
        previousPage: () => {
            store.dispatch(decreasePage());
        },
        nextPage: () => {
            store.dispatch(increasePage());
        },
        previousApiPage: () => {
            store.dispatch(decreaseApiPage());
        },
        nextApiPage: () => {
            store.dispatch(increaseApiPage());
        },
        getFirstItem: (total) => {
            if (!total) {
                return 0;
            }
            const { page } = store.getState().starships;
            return (page - 1) * PAGE_SIZE || 1;
        },
        getLastItem: (total) => {
            const { page } = store.getState().starships;
            const lastItem = page * PAGE_SIZE;
            return lastItem > total ? total : lastItem;
        },
        resetPaginator: () => {
            store.dispatch(resetPaginator());
        },
        setSearchTerms: (value) => {
            starshipManager().resetPaginator();
            store.dispatch(setSearchTerms(value));
        },
        clearSearchTerms: () => {
            starshipManager().resetPaginator();
            store.dispatch(setSearchTerms(''));
        },
        setOrderCriteria: (value) => {
            starshipManager().resetPaginator();
            store.dispatch(setOrderCriteria(value));
        },
        parseId: (url) => {
            return url.split('/').at(-2);
        }
    }
}
