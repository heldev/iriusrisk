import {
    Link
} from 'react-router-dom';
import './style.scss';

function Menu() {
    return (
        <nav className="menu">
            <ul className="menu__list">
                <li className="menu__item">
                    <Link to="/planets">Planets</Link>
                </li>
                <li className="menu__item">
                    <Link to="/starships">Starships</Link>
                </li>
            </ul>
        </nav>
    );
}

export default Menu;
