function Search({ value, onChange, clearSearch }) {
    return (
        <div className="search">
            <input type="text" className="search__input" value={value} onChange={({ target}) => onChange(target.value)} />
            <button className="clear-search__input" onClick={clearSearch}>x</button>
        </div>
    );
}

export default Search;
