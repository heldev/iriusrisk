import { useEffect, useState } from 'react';

function Sort({ options, onChange }) {

    const [optionsList, setOptionsList] = useState([]);

    useEffect( () => {
        setOptionsList(
            () =>
                options
                    .map(
                        ({key, label}) =>
                            <option className="filter__option" key={key} value={key}>{label}</option>
                    )
        );
    }, [options]);

    return (
        <div className="filter">
            <select onChange={({ target}) => onChange(target.value)}>
                { optionsList }
            </select>
        </div>
    );
}

export default Sort;
