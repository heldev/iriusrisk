import { useEffect, useState } from 'react';
import ProductCard from 'components/ProductCard';
import './style.scss';

function ProductGrid({ items }) {

    const [itemsList, setItemsList] = useState([]);

    useEffect( () => {
        setItemsList(
            () =>
                items
                .map(
                ({id, name, image, url}) =>
                    <ProductCard title={name} image={image} key={id} url={url} />
            )
        );
    }, [items]);

    return (
        <div className="product-grid">
            <ul className="product-grid__grid">{itemsList}</ul>
        </div>
    );
}

export default ProductGrid;
