import './style.scss';

function ProductCard({ title, image, url }) {

    const addDefaultSrc = e => {
        e.currentTarget.src = 'images/error-image.jpg';
    }

    return (
        <li className="product-card">
            <h3 className="product-card__image">{title}</h3>
            <img
                src={image}
                alt={title}
                onError={addDefaultSrc}
                className="product-card__image"
            />
            <a className="product-card__link" href={url} target="_blank" />
        </li>
    );
}

export default ProductCard;
