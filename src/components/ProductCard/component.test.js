import { render, screen } from '@testing-library/react';
import ProductCard from './';

test('renders the product card', () => {
  const { container } = render(<ProductCard />);
  const productCard = container.querySelector('li.product-card');
  expect(productCard).toBeInTheDocument();
});

test('renders the product card attributes', () => {
  const title = 'title-foo';
  const image = 'image-foo';
  const id = 1;
  const url = 'assets/images/foo/1.png'
  const { container } = render(<ProductCard title={title} image={image} key={id} url={url} />);
  const titleText = screen.getByText(title);
  const imageElement = container.querySelector(`[src="${image}"]` );
  const linkElement = container.querySelector(`[href="${url}"]` );
  expect(titleText).toBeInTheDocument();
  expect(imageElement).toBeInTheDocument();
  expect(linkElement).toBeInTheDocument();
});


