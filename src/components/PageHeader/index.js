import './style.scss';

function PageHeader({ title, children }) {
    return (
        <header className="page-header">
            <h2 className="page-header--subtitle">
                Imperials Destroyer Center
            </h2>
            <h1 className="page-header--title">
                {title}
            </h1>
            {children}
        </header>
    );
}

export default PageHeader;
