import './style.scss';

function disablePrev(first) {
    return first === 1;
}

function disableNext(last, total) {
    return last >= total;
}

function Paginator({ prevAction, nextAction, itemsName, first, last, total }) {
    return (
        <div className="paginator">
            <button className="paginator__previous-page" onClick={prevAction} disabled={disablePrev(first)}>◀</button>
            <span className="paginator__legend">{first} to {last} of {total} {itemsName}</span>
            <button className="paginator__next-page" onClick={nextAction} disabled={disableNext(last, total)}>▶</button>
        </div>
    );
}

export default Paginator;
