import {
    Routes,
    Route,
    Navigate
} from 'react-router-dom';
import Menu from 'components/Menu';
import Planets from 'pages/planets';
import Starships from 'pages/starships';
import './style.scss';

function Layout() {
    return (
        <div className="wrapper">
            <Menu />
            <Routes>
                <Route path="/planets" element={<Planets />} />
                <Route path="/starships" element={<Starships />} />
                <Route path="" element={<Navigate to="/planets" />} />
            </Routes>
        </div>
    );
}

export default Layout;
