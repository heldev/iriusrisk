import { configureStore } from '@reduxjs/toolkit'
import starshipReducer from 'domain/starship/store/starshipSlice';

export default configureStore({
    reducer: {
        starships: starshipReducer
    }
})
