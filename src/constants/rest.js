const REST_ACTIONS = {
    GET_STARSHIPS: 'https://swapi.dev/api/starships/'
}

const PAGE_SIZE = 12;

export {
    REST_ACTIONS,
    PAGE_SIZE
};
